#!groovy
 
import jenkins.model.*
import hudson.security.*
import jenkins.security.s2m.AdminWhitelistRule
 import hudson.security.csrf.DefaultCrumbIssuer

def instance = Jenkins.getInstance()
 
def hudsonRealm = new HudsonPrivateSecurityRealm(false)
hudsonRealm.createAccount("admin", "admin")
instance.setSecurityRealm(hudsonRealm)
 
def strategy = new FullControlOnceLoggedInAuthorizationStrategy()
instance.setAuthorizationStrategy(strategy)
instance.save()

instance.getInjector().getInstance(AdminWhitelistRule.class).setMasterKillSwitch(false)
instance.getDescriptor("jenkins.CLI").get().setEnabled(false)
instance.setCrumbIssuer(new DefaultCrumbIssuer(true))